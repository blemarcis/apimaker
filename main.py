__author__ = 'Baptiste Lemarcis'
from mysql import connector
import os
import wget
import zipfile


class ServeurMaker:

    @staticmethod
    def generate_serveur():
        url = "https://bitbucket.org/blemarcis/framework_node/src/e2520ff091f12c2aeb4dd3b220d8fc425f053cd1/" \
              "serveur.zip?at=master"
        filename = wget.download(url)
        with zipfile.ZipFile(filename, "r") as z:
            z.extractall("./")


class ModelsMaker:
    __models_directory = "/models/"
    __cnx = None
    __tables_name = []
    __models = {}
    __bd_array = {}
    __tables_references = {}
    __array_decode = {
        # Integer
        "TINYINT": "int",
        "SMALLINT": "int",
        "MEDIUMINT": "int",
        "INT": "int",
        "BIGINT": "int",
        "BIT": "int",
        # Real
        "FLOAT": "float",
        "DOUBLE": "float",
        "DECIMAL": "float",
        # Text
        "CHAR": "string",
        "VARCHAR": "string",
        "TINYTEXT": "string",
        "TEXT": "string",
        "MEDIUMTEXT": "string",
        "LONGTEXT": "string",
        # Binary
        "BINARY": "binary",
        "VARBINARY": "binary",
        "TINYBLOB": "binary",
        "BLOB": "binary",
        "MEDIUMBLOB": "binary",
        "LONGBLOB": "binary",
        # Temporal
        "DATE": "date",
        "TIME": "time",
        "YEAR": "int",
        "DATETIME": "datetime",
        "TIMESTAMP": "int",
        # Other
        "ENUM": "array",
        "SET": "array"
    }

    def __init__(self, user, password, host, db):
        self.__cnx = connector.connect(user=user, password=password,
                                       host=host,
                                       database=db)

    def __get_type(self, encoded_type):
        if encoded_type.find("(") != -1:
            encoded_type = encoded_type[:encoded_type.find("(")]
        return self.__array_decode[encoded_type]

    def __get_table_name(self):
        cursor = self.__cnx.cursor()
        cursor.execute("SHOW TABLES;")
        for name in cursor:
            self.__tables_name.append(str(name)[2:-3])
        cursor.close()

    def __create_models(self):
        cursor = self.__cnx.cursor()
        for table_name in self.__tables_name:
            cursor.execute("SHOW COLUMNS FROM " + table_name + ";")
            self.__bd_array[table_name] = []
            self.__models[table_name] = "module.export = Waterline.Collection.extend({ \n"
            self.__models[table_name] += "\ttableName: '" + table_name + "', \n"
            self.__models[table_name] += "\tschema: true, \n"
            self.__models[table_name] += "\tadapter: 'mysql', \n"
            self.__models[table_name] += "\tattributes: { \n"
            for (column_field, column_type, column_null, column_key, column_default, extra) in cursor:
                is_ref = False
                for ref in self.__tables_references[table_name]:
                    if column_field == ref:
                        is_ref = True
                if is_ref:
                    self.__models[table_name] += "\t\t\t" + "type : " +\
                                                            self.__get_type(column_type.upper()) + ",\n"
                else:
                    self.__models[table_name] += "\t\t" + column_field + " : {\n"
                    self.__models[table_name] += "\t\t\t" + "type : " +\
                                                            self.__get_type(column_type.upper()) + ",\n"
                    self.__models[table_name] += "\t\t\t" + "required : " +\
                                                            ("false" if str(column_null) == "NO" else "true") + "\n\t\t},\n"

                toto = "user: {"
                toto += "columnName: 'user_id',"
                toto += "type: 'integer',"
                toto += "foreignKey: true,"
                toto += "references: 'user',"
                toto += "on: 'id'"
                toto += "}"
                self.__bd_array[table_name].append([column_field, column_type, column_null,
                                                                column_key, column_default, extra])
            self.__models[table_name] = self.__models[table_name][:-2] + "\n"
            self.__models[table_name] += "\t}\n});"

            print(self.__models[table_name])
        cursor.close()

    def __get_joins(self):
        cursor = self.__cnx.cursor()
        for key in self.__bd_array.keys():
            cursor.execute("SELECT TABLE_NAME,COLUMN_NAME, REFERENCED_TABLE_NAME,REFERENCED_COLUMN_NAME from"
                           " INFORMATION_SCHEMA.KEY_COLUMN_USAGE where TABLE_NAME='" + key + "'")
            for (base_name, base_column, reference_name, reference_column) in cursor:
                if str(reference_name) != "None":
                    self.__tables_references[base_name].append([base_column, reference_name, reference_column])
                    print(self.__tables_references)
        print(self.__bd_array.keys())

    def __save_models(self):
        pass

    def __close_db(self):
        self.__cnx.close()

    def create_api(self):
        self.__get_table_name()
        self.__get_joins()
        self.__create_models()
        self.__save_models()
        self.__close_db()


class ControlleursMaker:
    pass


def main():
    ServeurMaker.generate_serveur()
    run = ModelsMaker("root", "root", "127.0.0.1", "apimaker")
    run.create_api()

if __name__ == '__main__':
    main()
